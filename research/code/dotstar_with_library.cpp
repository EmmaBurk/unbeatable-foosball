// Turn off the dotstar/set the dotstar's color.

#include <Arduino.h>
#include <Adafruit_DotStar.h>
#include <Adafruit_GFX.h>
#include <SPI.h>
#include <stdio.h>

#define NUMPIXELS 1
#define DATAPIN 7
#define CLOCKPIN 8
Adafruit_DotStar strip = Adafruit_DotStar(NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BGR);

void setup() {
  // put your setup code here, to run once:
  strip.begin();
  strip.show();
  strip.setPixelColor(0, 0x000001); // blue
  strip.show();
  delay(1000);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(13, 0);
  delay(1000);
  analogWrite(13, 0);
  delay(1000);
  strip.setPixelColor(0, 0x10FF4F); // blue 0000 1111 0000 1111 0000 1111
  strip.show();
  delay(1000);
}
