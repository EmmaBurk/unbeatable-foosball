// UART Communication 
#define FOSC 16000000 // 16MHz clock
#define BAUD 9600     // Transmit baud rate
#define MYUBRR FOSC/16/BAUD-1

// Function Prototypes
void USART_Transmit(const char* letters);
void USART_Transmit(uint8_t DataByte, const char* type);
void USART_Transmitln(const char* letters);
void USART_Transmitln(uint8_t DataByte, const char* type);
void bin_to_hex(uint8_t data);
void USART_Transmit_ASCII(uint8_t DataByte);
void USART_Init(unsigned int);
int twos_power(int power);

// Put baud rate into higher and lower registers
#define UBRR0H *((volatile byte*) 0xC5)
#define UBRR0L *((volatile byte*) 0xC4)

// Enable transition and receiving
#define UCSR0B *((volatile byte*) 0xC1) // USART control and status register 0 B
#define RXEN0  4                        // When bit is high USART receiver enabled
#define TXEN0  3                        // When bit high USART transmition enabled

// Set frame Format
#define UCSR0C *((volatile byte*)0xC2)  // USART control and status register 0 C
#define UCSZ00 1                        // When set high then send 8 bits

#define UCSR0A *((volatile byte*) 0xc0) // USART control and status register 0 A
#define UDRE0  5                        // Data Register Empty

#define BIN 'bin'
#define HEX 'hex'

void USART_Init(unsigned int ubrr) {
  // Set baud rate
  UBRR0H = (unsigned char) (ubrr >> 8);
  UBRR0L = (unsigned char) ubrr;
  // Enable both transmit and receive
  UCSR0B = (1<<RXEN0) | (1<<TXEN0);
  // Set frame format: 8data, no parity, 1 stop bit
  UCSR0C = (3<<UCSZ00);
}

void USART_Transmitln(const char* letters) {
  USART_Transmit(letters);
  USART_Transmit_ASCII('\n'); 
}

void USART_Transmitln(uint8_t DataByte, const char* type = 'none') {
  USART_Transmit(DataByte, type);
  USART_Transmit_ASCII('\n');
}


void USART_Transmit(const char* letters) {
  for(int i = 0; i < strlen(letters); i++) {
     USART_Transmit_ASCII(letters[i]);
  }
}

void USART_Transmit(uint8_t DataByte, const char* type = 'none') {
  if (type == BIN) {
    for (int i = 7; i >= 0; i--) {
      if (i==3) {
        USART_Transmit_ASCII(' ');
      }
      if (DataByte & (1 << i)) { USART_Transmit_ASCII('1'); }
      else { USART_Transmit_ASCII('0'); }
    }
  }
  else if (type == HEX) {
    bin_to_hex(DataByte);
  }
  else {
    USART_Transmit(type);
  }
}

void bin_to_hex(uint8_t data) {
  int data_ten_first = 0;
  int data_ten_last = 0;
  for (int i = 7; i >=0; i--) {//1000 0000
    if ((data & (1 << i)) && i >= 4) {
      data_ten_first += twos_power((i%4));
    }
    else if (data & (1 << i)) {
      data_ten_last += twos_power((i%4));
    }
  }
  
  if (data_ten_first > 9) {
    data_ten_first += 87;
  }
  else {
    data_ten_first += 48;
  }
  if (data_ten_last > 9) {
    data_ten_last += 87;
  }
  else {
    data_ten_last += 48;
  }

  USART_Transmit("0x");
  USART_Transmit_ASCII(data_ten_first);
  USART_Transmit_ASCII(data_ten_last);
}

int twos_power(int power) {
  int product = 2;
  if (power == 0) {
    product = 1;
  }
  for (int i=2; i <= power; i++) {
    product *= 2;
  }
  return product;
}

void USART_Transmit_ASCII(uint8_t DataByte) {
  while ((UCSR0A & (1 << UDRE0)) == 0) {};
  UDR0 = DataByte;

  // Wait until UDR is ready
  // The UDREn flag indicates if the transmit buffer (UDRn) is 
  // ready to receive new data. If UDREn is one, the buffer is empty,
  // and therefore ready to be written. The UDREn flag can generate a 
  // data register empty interrupt (see description of the UDRIEn bit). 
  // UDREn is set after a reset to indicate that the transmitter is ready
}
