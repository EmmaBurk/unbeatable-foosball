// This is supposed to read information from the accelerometer but it hasn't. 


#include "serialMonitor.h"
#include "SPI.h"

int main(void){
  // Set UART
  USART_Transmitln("USART init");
  USART_Init(MYUBRR);

  USART_Transmitln("start init");
  SPI_MasterInit();


  USART_Transmitln("start");
  
  PORTB |= (1<<SS);
  PORTB &= ~(1<<SS);

  USART_Transmitln("Transmit first");
  SPI_MasterTransmit(0x20);
  SPI_MasterTransmit(0x8F);
  USART_Transmitln("Transmit end");

  PORTB |= (1<<SS);
  PORTB &= ~(1<<SS);

  USART_Transmitln("Transmit second");
  SPI_MasterTransmit(0x8F);
  SPI_MasterTransmit(0x00);
  USART_Transmitln("Transmit end");

  USART_Transmitln(SPI_MasterReceive(), BIN);
  
  PORTB &= ~(1<<SS);
  
  while(1){
  }
}

void SPI_MasterTransmit(uint8_t cData) {
  PORTB |= (1<<SS);
  /* Start transmission */
  SPDR = cData;
  /* Wait for transmission to complete. */
  while(!(SPSR & (1<<SPIF)))
  ;
}

uint8_t SPI_MasterReceive(void) {
  return SPDR;
}

void SPI_MasterInit(void){
  // Enable global interrupts
  //SREG |= (1<<BIT_I);
  
  /* Set MOSI and SCK output, all others input */
  DDRB = (1 << MOSI) | (1 << SCK) | (1 << SS);
  
  /*Enable Interrupts, Enable SPI, Master, Set clock rate fck/16 */
  SPCR = (1<< SPE) | (1 << MSTR) | (1<<SPR0);
}
