// Master in slave out MISO // 125

// Function Prototypes
void SPI_MasterInit(void);
void SPI_MasterTransmit(uint8_t);
uint8_t SPI_MasterReceive(void);

// SPI Communication

// SPI Serial Transfer Complete
#define SPI_STC_vect _VECTOR(17)

// Port B
#define DDRB  *((volatile byte*) 0x24) // Data direction register
#define PINB  *((volatile byte*) 0x23) // Input Pins Address
#define PORTB *((volatile byte*) 0x25) // Data Register

// The AVR status register
#define SREG *((volatile byte*) 0x5F) 
#define BIT_I 7 // Set global interrupt enable 

// SS is reversed logic on adafruit's documentation it says to 'drop the pin low'
// or reset SS to start an SPI transaction. Set it high here. 
#define SS   2 // Set as input  on DDRB 
#define MOSI 3 // Set as output on DDRB
#define MISO 4 // Set as input  on DDRB 
#define SCK  5 // Set as output on DDRB

// PRSPI bit must be written to zero to enable SPI
// initial value is already zero 
#define PRR *((volatile byte*) 0x64) // Power reduction register
#define PRSPI 2                      // Power reduction SPI - set high stops clock to module for SPI

// SS - slave select must be selected for master before communication - is not auto
// Writing a byte to the SPI data register starts the SPI clock generator and the
// hardware shifts the eight bits to the slave. When byte has been passed then SPIF
// is raised high. 

// Set   = 1
// Reset = 0

// SPI control register
#define SPCR *((volatile byte*) 0x4C)
#define SPIE 7 // SPI Interrupt Enable
#define SPE  6 // SPI communication enabled when set
#define MSTR 4 // Master SPI mode when set
#define SPR0 0 // Controls SCK frequency

// SPI Status Register
#define SPSR *((volatile byte*) 0x4D)
#define SPIF 7 // Interrupt Flag set when serial transfer is complete

// SPI Data Register
#define SPDR *((volatile byte*) 0x4E) // writing to the register initiates data transmission

// SS needs to be configured as an input
