/* This code hasn't been tested on the arduino but it does not work on the Trinket M0 
 * Part of the problem is that the library doesn't work for the Trinket M0 and I can't 
 * remember why not but it wouldn't work for me. This code may be appropriate for the arduino.
 * http://www.energiazero.org/arduino_sensori/how2electronics.com-How%20to%20use%20Reyax%20RYLR890%20LoRa%20Module%20with%20Arduino.pdf
*/

#include <Arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial lora(1, 4);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  lora.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  String inString;
  while(lora.available()){
    if(lora.available()){
      inString += String(char(lora.read()));
    }
  }
  if(inString.length() > 0) {
    String potval;
    potval=inString.substring(9,12);
    Serial.println(potval);
    analogWrite(LED_BUILTIN, potval.toInt());
  }
}
