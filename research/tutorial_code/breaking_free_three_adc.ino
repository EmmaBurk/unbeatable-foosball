// Hook up a rotery encoder with S1 in ADC channel 0

#define FOSC 16000000 // 16MHz clock
#define BAUD 9600     // Transmit baud rate
#define MYUBRR FOSC/16/BAUD-1

// Function Prototypes
void USART_Transmit(uint8_t);
void USART_Init(unsigned int);

// Put baud rate into higher and lower registers
#define UBRR0H *((volatile byte*) 0xC5)
#define UBRR0L *((volatile byte*) 0xC4)

// Enable transition and receiving
#define UCSR0B *((volatile byte*) 0xC1) // USART control and status register 0 B
#define RXEN0  4                        // When bit is high USART receiver enabled
#define TXEN0  3                        // When bit high USART transmition enabled

// Set frame Format
#define UCSR0C *((volatile byte*)0xC2)  // USART control and status register 0 C
#define UCSZ00 1                        // When set high then send 8 bits

#define UCSR0A *((volatile byte*) 0xc0) // USART control and status register 0 A
#define UDRE0  5                        // Data Register Empty

#define TIMER0_COMPA_vect _VECTOR(14)

char myString[11] = {'A', 'T', 'm', 'e', 'g', 'a', '3', '2', '8', 'P', '\0'}; 
int counter = 0;
volatile uint8_t counter =0;


int main(void) {
  Timer0_Init();
  ADC_Init();

  while(1) {
    if(counter >= 125) { // 4ms * 125 = 500ms
      temp = ADC_Read(0); // Read ADC channel 0

      ADC0_Result_ONES = temp % 10;
      temp = temp / 10;
      ADC0_Result_Tens = temp % 10;
      temp = temp / 10;
      ADC0_Result_Hundreds = temp % 10;
      ADC0_Result_Thousands = temp / 10;

      // Convert to ASCII values, adds 0x30 to each
      ADC0_Result_Thousands += 0x30;
      ADC0_Result_Hundreds += 0x30;
      ADC0_Result_Tens += 0x30;
      ADC0_Result_Ones += 0x30;

      // Wrtie the ADC reading to the USART
      USART_TransmitByte(ADC0_Result_Thousdands);
      USART_TransmitByte(ADC0_Result_Hundreds);
      USART_TransmitByte(ADC0_Result_Tens);
      USART_TransmitByte(ADC0_Result_Ones);
      USART_TransmitByte('\n');

      // Reset the counter
      counter = 0;
    }
}
void USART_Init(unsigned int ubrr) {
  // Set baud rate
  UBRR0H = (unsigned char) (ubrr >> 8);
  UBRR0L = (unsigned char) ubrr;
  // Enable both transmit and receive
  UCSR0B = (1<<RXEN0) | (1<<TXEN0);
  // Set frame format: 8data, no parity, 1 stop bit
  UCSR0C = (3<<UCSZ00);
}

void USART_Transmit(uint8_t DataByte) {
  // Wait until UDR is ready
  // The UDREn flag indicates if the transmit buffer (UDRn) is 
  // ready to receive new data. If UDREn is one, the buffer is empty,
  // and therefore ready to be written. The UDREn flag can generate a 
  // data register empty interrupt (see description of the UDRIEn bit). 
  // UDREn is set after a reset to indicate that the transmitter is ready
  
  while ((UCSR0A & (1 << UDRE0)) == 0) {};
  UDR0 = DataByte;
}

// Timer0 overflow interrupt
ISR (TIMER0_COMPA_vect)  {
  counter++;
}

ISR(ADC_vect) // ADC interrupt{
}

uint16_t ADC_Read(char channel) {
  // Select channel requested
  ADMUX = (1<<REFS0) | (channel & 0x0f);
  
  // Start conversion
  ADCSRA |= (1 << ADSC);
  
  // Wait for results
  while (ADCSRA & (1<<ADSC));
  return ADCW;
}

// REFS1 low and REFS0 high = AVcc with external capacitor at AREF pin
#define ADMUX *((volatile byte*) 0x7C) // ADC Multi Selection Register
#define REFS0 6                        // Reference selection bits

#define ADCSRA *((volatile byte*) 0x7A) // ADC control and status register A
#define ADSC   6                        // Write high to start conversion
                                        // will auto low when conversion finished
#define ADEN   7                        // Writing this bit to one enables the ADC
// These bits determine the division factor between the system clock frequency 
// and the input clock to the ADC
#define ADPS0  0                        // ADPS2:0 ADC Prescaler select bits
#define Division_factor 7               // Sets division factor to 128 

#define ADCW *((volatile byte*) 0x78)   // 10 bit word?


void ADC_Init() {
  // Uses AVCC which is connected to +5V on UNO
  ADMUX = (1 << REFS0);

  // Clear 4 LSB to set ADC0 (A0 on Arduino)
  // Bits 3:0 - MUX3:0 Analog channel selction bits
  // The value of these bits selects which analog inputs are connected to the ADC
  ADMUX &= 0xF0;

  // Set ADC clock to 125kHz and enable ADC
  ADCSRA |= (1 << ADEN) | (Divison_factor<<ADPS0); 
}

void Port_Init() {
  // Set LED as output
  DDRB |= (1<<PD5);
}

#define TCCR0A *((volatile byte*) 0x44) // Timer/Counter control register set mode to...
#define WGM01  1                        // CTC mode

#define OCR0A *((volatile byte*) 0x47)  // Output compare register A
#define TOP_VALUE 249                    // Reset timer at 249

#define TIMSK0 *((volatile byte*) 0x6E) // Timer/counter interrupt mask register
#define OCIE0A 1                        // Enable timer/counter compare match A interrupt

#define TCCR0B *((volatile byte*) 0x45) //Timer/Counter control register B
#define CS02   2                        // Set prescaler to 256

void Timer0_Init() {
  // Set timer mode to CTC
  TCCR0A |= (1 << WGM01);

  // Set the value that we want to count to (249)
  OCR0A = 0xF9;

  // Set the ISR COMPA vect
  TIMSK0 |= (1<<OCIE0A)

  // Enable global interrupts
  sei();

  // Set the prescaler to 256 and start timer
  TCCR0B = (1 << CS02);
}
