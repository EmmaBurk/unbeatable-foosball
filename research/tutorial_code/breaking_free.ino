// Blinks the light on the arduino

#define DATA_DIR *((volatile byte*) 0x24)
#define PB_INPUT *((volatile byte*) 0x23)
#define PB_DATA *((volatile byte*) 0x25)

// TCCR0A bit 1 is for WGM01 Set timer to CTC mode
// CTC = Clear Timer on Compare (compares with OCR0A)
#define TCCR0A *((volatile byte*) 0x44)
// Set to 1
#define WGM01 1

// OCR0B set to 249
#define OCR0A *((volatile byte*) 0x47) 
#define TOP_VALUE 249

// Output compare interrupt enable timer 0
// set OCIE0B high to enable compare match A interrupt
#define TIMSK0 *((volatile byte*) 0x6E)
#define OCIE0A 1

// Set prescaler for clock to 256 and start timer!
#define TCCR0B *((volatile byte*) 0x45)
// Set high
#define CS02 2

// Interrupt vector location 
#define TIMER0_COMPA_vect _VECTOR(14)

//Delay
volatile uint8_t counter = 0;

#define LED 5

int main(void) {
  // put your setup code here, to run once:
  DATA_DIR |= (1 << LED);
    
  // Set timer to CTC mode for frequency
  TCCR0A |= (1 << WGM01);

  // Set overflow comparison to 249 as per frequency eq. 
  OCR0A = TOP_VALUE;

  // Se the ISR COMPA vect
  TIMSK0 |= (1 << OCIE0A);

  // Enable global interrupts
  sei();
  
  // Set prescaler to 256 for frequency eq to work and start timer
  TCCR0B |= (1 << CS02);
  
  while (1) {
    if (counter >= 125){
      if (PB_DATA && (1 << LED)){
        PB_DATA &= ~(1 << LED);
      }
      else {
        PB_DATA |= (1 << LED);
      }
      counter = 0;
    }
  }
}

// timer0 compare A register interrupt
ISR (TIMER0_COMPA_vect) {
  counter++;
}
