// Set up button on pin 5
// Button switches the light on and off

// function prototypes
uint16_t RawKeyPressed(void);
bool DebounceSwitch(void);

#define DDRB   *((volatile byte*) 0x24) // Data direction register Port B
#define DDB5   5                        // Light pin on arduino

#define DDRD   *((volatile byte*) 0x2A) // Data direction register Port D
#define PORTD  *((volatile byte*) 0x2B) // Data Register Port D
#define PIND   *((volatile byte*) 0x29) // Input pins address Port D
#define PD5    5                        // Port 5 for GPIO pin 5
#define DDD5   5                        // GPIO pin 5 (D5)

#define TCCR0A *((volatile byte*) 0x44) // Timer/Counter control register set mode to...
#define WGM01  1                        // CTC mode

#define OCR0A *((volatile byte*) 0x47)  // Output compare register A
#define TOP_VALUE 62                    // Reset timer at 62

#define TIMSK0 *((volatile byte*) 0x6E) // Timer/counter interrupt mask register
#define OCIE0A 1                        // Enable timer/counter compare match A interrupt

#define TCCR0B *((volatile byte*) 0x45) //Timer/Counter control register B
#define CS02   2                        // Set prescaler to 256

#define TIMER0_COMPA_vect _VECTOR(14)

int main(void) {
  // Set Arduino LED as output
  DDRB |= (1 << DDB5);

  // Set Arduino digital pin 5 as input
  DDRD &= ~(1 << DDD5);  
  
  // Turn on input pullup
  PORTD |= (1 << PD5);


  // Set up Timer/Counter 0 for 1ms interrupts
  // Set timer Mode to CTC
  TCCR0A |= (1 << WGM01);
    
  // Set the value that we want TC0 (timer/counter 0) to count to (62)
  OCR0A = 62;
  
  // Set the ISR COMPA vect - enable compare match A
  TIMSK0 |= (1 << OCIE0A); 
  
  // Enable global interrupts
  sei();
  
  // Set prescaler to 256 and start the timer
  TCCR0B |= (1 << CS02);

  while(1) {
  }
}

//this ISR gets called every 1ms
ISR (TIMER0_COMPA_vect) // timer0 compare A register interrupt
{
  if (DebounceSwitch()) {
    PORTB ^= 1 << 5;
  } 
}

bool DebounceSwitch() {
  // Current debounce status - this is a function variable that 
  // remembers what it was last set to and is not initialized with
  // every new function call. Pretty cool!
  static uint16_t State = 0;
  
  // Button bouncing makes number below look like 1111 0000 0000 0001
  State = (State << 1) | RawKeyPressed() | 0xe000;

  // If the switch hasn't been randomly switching then no ones are in the wrong spot
  // and they are only in the last four bits 1111 0000 0000 0000
  if (State == 0xf000) { // End condition - read the last bounce.
    return true;
  }
  return false;
}

uint16_t RawKeyPressed() {
  // Get inputs and shift to them to the end of the 16 bits
  return (PIND & 1<<PD5) >> PD5;
}


// https://www.youtube.com/watch?v=LzOdcKpJkDw&t=344s
