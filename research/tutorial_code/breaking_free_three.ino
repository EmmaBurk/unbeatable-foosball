// Open Serial Monitor and run - should spam ATmega329P

#define FOSC 16000000 // 16MHz clock
#define BAUD 9600     // Transmit baud rate
#define MYUBRR FOSC/16/BAUD-1

// Function Prototypes
void USART_Transmit(uint8_t);
void USART_Init(unsigned int);

// Put baud rate into higher and lower registers
#define UBRR0H *((volatile byte*) 0xC5)
#define UBRR0L *((volatile byte*) 0xC4)

// Enable transition and receiving
#define UCSR0B *((volatile byte*) 0xC1) // USART control and status register 0 B
#define RXEN0  4                        // When bit is high USART receiver enabled
#define TXEN0  3                        // When bit high USART transmition enabled

// Set frame Format
#define UCSR0C *((volatile byte*)0xC2)  // USART control and status register 0 C
#define UCSZ00 1                        // When set high then send 8 bits

#define UCSR0A *((volatile byte*) 0xc0) // USART control and status register 0 A
#define UDRE0  5                        // Data Register Empty

char myString[11] = {'A', 'T', 'm', 'e', 'g', 'a', '3', '2', '8', 'P', '\0'}; 

int main(void) {
  USART_Init(MYUBRR);

  while(1) {
    for(int i = 0; i < strlen(myString); i++) {
      USART_Transmit(myString[i]);
    }
    USART_Transmit('\n');
  }
  return 0;
}
void USART_Init(unsigned int ubrr) {
  // Set baud rate
  UBRR0H = (unsigned char) (ubrr >> 8);
  UBRR0L = (unsigned char) ubrr;
  // Enable both transmit and receive
  UCSR0B = (1<<RXEN0) | (1<<TXEN0);
  // Set frame format: 8data, no parity, 1 stop bit
  UCSR0C = (3<<UCSZ00);
}

void USART_Transmit(uint8_t DataByte) {
  // Wait until UDR is ready
  // The UDREn flag indicates if the transmit buffer (UDRn) is 
  // ready to receive new data. If UDREn is one, the buffer is empty,
  // and therefore ready to be written. The UDREn flag can generate a 
  // data register empty interrupt (see description of the UDRIEn bit). 
  // UDREn is set after a reset to indicate that the transmitter is ready
  
  while ((UCSR0A & (1 << UDRE0)) == 0) {};
  UDR0 = DataByte;
}
