# How to Get Started
If you're like me then you want to program things at a lower lever - or maybe you have to. I had a really hard time getting into it because I started with the Adafruit Trinket M0, which doesn't have a lot of documentation on lower-level programming. So this beginning part will focus on the Arduino because it is one of the most well documented and the most researched microcontrollers. 

## Tutorials
To get started you should watch these YouTube tutorials:
* [Blink with interrupts - Part 1](https://www.youtube.com/watch?v=hX5k1OWqCtg&t=1704s)
* [Arduino Blink](https://www.youtube.com/watch?v=tBq3sO1Z-7o&list=PLNyfXcjhOAwOF-7S-ZoW2wuQ6Y-4hfjMR&index=1) - watch the second part too!
* [Read Button input - Part 2](https://www.youtube.com/watch?v=LzOdcKpJkDw)
* [USART and ADC - Part 3](https://www.youtube.com/watch?v=tLpXoEtBhK4&t=2773s)

Parts 1-3 were created by a very well studied guide, he explains the documentation and his instruction makes it easier to understand the documentation for the Arduino ATmega328P. The Arduino Blink video will show you how to write to a port address. You may elect to use the AVR library as reccommened in Parts 1-3, however, this will slow you personal learning of the datasheet and you should look it up yourself. 

## Finding the port address
Open the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf) for the ATmega328P - this is the microcontroller that is soddered onto your Arduino Uno. It is the brains of the Arduino and you can make your own 'Arduino' with just the Atmega328P, a timer, and a few other pieces. The Arduino is just a board made by a manufacturer to make it easier for hobbiests and students to access the ATmega328P. 

<img src="images/arduino_uno_board.png" alt="drawing" width="500"/>

[Image from Circuit Digest](https://circuitdigest.com/article/everything-you-need-to-know-about-arduino-uno-board-hardware)

In the datasheet, go to the section 'Ports' (p.58), this is the place to start on every datasheet. Scroll down until you see the register addresses (p.72), this is what they look like:

<img src="images/register_description.PNG" alt="drawing" width="750"/>


To the left of the boxes you can see a hex number 0x35(0x55). 0x35 is the relative address to the port's beginning address and 0x55 is the actual address. We can find the port's beginning address from the datasheet in the AVR Memories SRAM Data Memory section:

<img align="left" src="images/i_o_memorys.PNG" alt="drawing" width="250"/>

    Note: The port is referring to I/O Ports but some datasheets just refer to I/O register as the port. 
    Recommendation: Search 0x20 and look for other places in the data sheet that give you the port's beginning address. 

The beginning address is 0x20 - so to find the absolute address for a register in the port you have to add 0x20 to the relative addresses. For address 0x35 you add 0x20 to get 0x55 - the number in the parenthesis! 

<br clear="left"/>
So this datasheet is super helpful and gives you the absoulte address in parentheses next to the relative address but some datasheets do not do that - so you should remember this. If there is only one address given, then only the absolute address was given - this is different for each datasheet!!! You have to read some of the documentation to find out - I watched the tutorials and reveiwed the [AVR library](https://github.com/vancegroup-mirrors/avr-libc/blob/master/avr-libc/include/avr/iom328p.h) to make sure that I had the correct address in the tutorials for parts 1-3. 

## Using the right pins
<img align="left" src="images/arduino_uno_pins.png" alt="drawing" width="500"/> Follow the key and use specifically the Microcontroller's Ports. PBx stands for Port B Pin x.

Image from [Arduino](https://store-usa.arduino.cc/products/arduino-uno-rev3)
<br clear="left"/>

## Tips and Tricks
* Don't look up sei() you won't find anything, but do look up 'sei' in the datasheet - it corrisponds to the interrupt enable on the Arduino. 
* On p.49 there is a list of interrupt vectors - your program will take the Vector No. in the first collum not the program address. * You can throw any vector no in a ISR (Vector No.) - interupt service routine - on the Arduino IDE. 
* You should read the section you're working on and then apply it. You might not understand it when you read it but you will begin understanding it when you apply it. 

## LIS3DH 
SPI communication with the Adafruit Accelerometer, the LIS3DH. A great reference is the [datasheet](https://www.st.com/resource/en/datasheet/lis3dh.pdf) so you should pull that up. Here is an image of section 6.2.1:
<img align="left" src="images/spi.PNG" alt="drawing" width="500"/>


The CS needs to be low as you can see on the timing diagram, or read in the documentation on page 27. Review the internal pin status (table 13) as well to see how the CS enables SPI communication. View Table 7 to view clock cycle and clock frequency. The clock is fed to the LIS3DH through the arduino and should be programmed accordingly.
<br clear="left"/>
Review Table 21 to view the Register address map, you will need this to program AD(5:0). I will be taking data from OUT_X_L address 0x28 or 0101000. It shows that this is an output address and I believe it will give me the data for the x direction. 

I'm still figuring out SPI communication and I will write a section on that. 


























