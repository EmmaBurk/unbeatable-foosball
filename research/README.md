# Arduino

* Datasheet
    * [Arduino ATmega328P](https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf)


* Diagrams
    * [Arduino Pin Diagram](https://www.google.com/search?q=arduino+uno+datasheet&sxsrf=ALiCzsZp_oQivi3vvJf_o-XWN-9WCd6hDQ:1660920267215&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjp65uUktP5AhWeKUQIHfOvBgEQ_AUoAXoECAEQAw&biw=767&bih=748&dpr=1.25#imgrc=v_BkiCVP1mkpXM)


* Bare-Metal Tutorials:
    * [Arduino Blink](http://electronicswithyou.com/arduino/led-blinking-code-with-and-without-using-arduino-library/)
    * [Arduino Blink - YouTube](https://www.youtube.com/watch?v=tBq3sO1Z-7o&list=PLNyfXcjhOAwOF-7S-ZoW2wuQ6Y-4hfjMR&index=1)
    * [Blink with interrupts - YouTube](https://www.youtube.com/watch?v=hX5k1OWqCtg&t=1704s)
    * [Read Button input](https://www.youtube.com/watch?v=LzOdcKpJkDw)
    * [USART and ADC](https://www.youtube.com/watch?v=tLpXoEtBhK4&t=2773s)

* Libraries
    * [AVR library - Port address names in Arduino library](https://github.com/vancegroup-mirrors/avr-libc/blob/master/avr-libc/include/avr/iom328p.h)


# Adafruit Accelerometer

* Datasheets
    * [LIS3DH Accelerometer](https://www.st.com/resource/en/datasheet/lis3dh.pdf)
    * [Adafruit Accelerometer](https://cdn-learn.adafruit.com/downloads/pdf/adafruit-lis3dh-triple-axis-accelerometer-breakout.pdf)

* Libraries
    * [Port address Names in Adafruit](https://github.com/arduino/ArduinoCore-samd/blob/master/bootloaders/mzero/Bootloader_D21/src/ASF/sam0/utils/cmsis/samd21/include/instance/port.h)

# Adafruit Trinket M0

* Datasheets
    * [Adafruit documentation](https://cdn-learn.adafruit.com/downloads/pdf/adafruit-trinket-m0-circuitpython-arduino.pdf)
    * [SAMD21E](https://cdn.sparkfun.com/datasheets/Dev/Arduino/Boards/Atmel-42181-SAM-D21_Datasheet.pdf)
    * [SAMD21](https://www.digchip.com/datasheets/download_datasheet.php?id=284674&part-number=ATSAMD21E18&type=pn2)
    * [Overview](https://docs.zephyrproject.org/2.6.0/boards/arm/adafruit_trinket_m0/doc/index.html)

* Diagrams
    * [Trinket Pin Diagram](https://learn.adafruit.com/assets/49778)
    * [Trinket Advanced](https://learn.adafruit.com/assets/78147)

* Tutorials
    * [Blink - Bare-Metal](https://github.com/konimarti/trinketm0/blob/master/blink.c)
    * [Circuit Python Example](https://github.com/adafruit/awesome-circuitpython/blob/main/cheatsheet/CircuitPython_Cheatsheet.md)

* Libraries
    * [Circuit Python how-to](https://learn.adafruit.com/adafruit-trinket-m0-circuitpython-arduino?view=all#circuitpython)
    * [Circuit Python Github](https://github.com/adafruit/circuitpython/tree/main/ports/atmel-samd)
    * [More Circuit Python](https://docs.circuitpython.org/en/latest/README.html)
    * [Adafruit_BusIO](https://github.com/adafruit/Adafruit_BusIO)

* Extra
    * [Discord for Trinket M0](https://discord.com/channels/327254708534116352/327298996332658690/439506794415652864)


# Communication Protocols
[SPI Overview - Read](https://blog.mbedded.ninja/electronics/communication-protocols/spi-communication-protocol/)

[SPI Overview - Watch](https://www.youtube.com/watch?v=xogsRnnhK44)

[SPI Actually Send a Byte](https://www.youtube.com/watch?v=sHmixFJhr3M)


# Extra Stuff
[Arduino IDE Modifications](https://github.com/per1234/ThemeTest)

[Adafruit Dotstar](https://cdn-shop.adafruit.com/product-files/2328/alt+sk9822+datasheet.pdf)

[Code Design Patterns](https://refactoring.guru/design-patterns/catalog)

[Arem instruction set](file:///C:/Users/emmal/Downloads/QRC0001_UAL.pdf)

[Bitwise Calculator](https://miniwebtool.com/bitwise-calculator/bit-shift/?data_type=2&number=1&place=10&operator=Shift+Left)

[Dec-hex-bin-oct-10 converter](https://www.rapidtables.com/convert/number/decimal-to-hex.html)

[Installing C++ debugger on VS code](https://code.visualstudio.com/docs/cpp/config-mingw)

[Practical Guide to Bare Metal C++](https://alex-robenko.gitbook.io/bare_metal_cpp/basic_concepts/device_driver_component)

[Dotstar Documentation](https://www.digikey.sg/htmldatasheets/production/1825800/0/0/1/3776.html)

[Bootloader](https://www.youtube.com/watch?v=BJe-pTgfLLM)
