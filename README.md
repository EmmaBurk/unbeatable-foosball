# Unbeatable Foosball

    Create an easy setup for computer domination on any foosball table.

### Different Aspects of Computer Domination:
* Create gyro foosball 
    - This 3D-printed-hardware-filled ball is the ball that will be used for game-play. It will connect with the computer and constantly report its location in two dimensions. Before the game has begun the user will roll the ball across the foosball table, next to the wall. The computer will take the velocity from the accelerometer which is inside the ball and use math to determine the length of the table. The user will do the same action for the width of the table. 
* Picture that identifys foosball men, box, and drop locations.
    - The 3D ball maps out the board's dimensions but it cannot identify the location of every foosball man. The camera will use machine learning to identify the location of the foosball men and then use mathematics to determine their position based on the ball's recorded dimensions. 
* Linear and circular bar movement
    - Moves the bar of foosball men to manipulate the ball's position and aim the ball toward the enemy box. This requires rotating the bar and pushing and pulling the bar. 
* Advanced Software
   - This includes strategies for the computer to implement and physics to track the suspected future location of the ball. Some other aspects of coding required in this project are machine learning for foosball men identification in pictures, networking to receive data from five different balls (4/5 balls are part of the stretch goals below) and send data to eight different servos, and general hardware integrated software.
* Enemy Tracking (Stretch Goal)
    - Enemy Tracking will attach to the end of the opponents bars and will track the position of their foosball men based on their bars' circular and linear movements. This will allow the software to track openings through all foosball men on the table and hit the ball into the enemy box everytime. Also, the computer could predict the opponent's strategies based on how they manipulate the direction of the ball. 
  
## Program Specifications:
- C Language
- Easy to install and uninstall modifications
    - If the modifications are difficult to remove and apply then you can't use the program on any foosball table and then the other interns will dislike me for ruining their foosball table permanently. 
- IDE: VS Code

### Materials:
* Reasonably flat Foosball Table
* (1) 3D Printed foosball
* (1) Gyro Accelerometer
* (1) Transceiver
* (1) Microcontroller
* (8) Servo
    - Type Undetermined
* Camera
* Arduino
    - M/M, F/M, and F/F Wires
* (1) Batteries


#### Materials for stretch goals:
* (4) 3D Printed foosball
* (4) Gyro Accelerometer
* (4) Transceiver
* (4) Microcontroller
* (4) Batteries


## # TO-DO
* Create gyro foosball 
    - 3D Print foosball
    - [Research gyro and accelerometer intergration and necessary powersources and how to send location to computer.](foosball_ball.md) 
    - Experiment with data received from gyro and accelerometer using arduino and computer
    - Install gyro and accelerometer and battery into foosball
    - Develop test cases for events such as:
        - The ball is hit too hard
        - The Foosball table is in range
        - The box and the drop location is in range
        - The ball gives sends accurate and relevent location information back to the computer
    - Create code based on test cases
    - Perform tests on code and fix gaps.
* Picture that identifies foosball men, box, and drop locations.
    - Research picture identification options
    - Software implementation.
    - Use Gyro foosball data to scale foosball table and create software 'map' which will be used for reporting to the bar movement part of the code.
* Linear and circular bar movement
    - Communicate with MEs on their plan
        - May require 3D printing to reduce need to screw into Foosball table.
    - Research software for servos
    - Implement hardware based on MEs's plans
    - Experiment with servos (both linear/Circular)
    - Create test cases for servos (L/C)
    - Create basic software for servos
    - Integrate with picture and gyro foosball to receive map and ball location so that it knows when to spin, push, and pull.
    - Test code again, refine code, and test again.
* Advanced Software
    - Research basic foosball strategies
    - Code appropriate testing software!
    - Write code reflective of basic strategies, testing each strategy before integrating it into the code
    - Use physics to predict future ball location! (Stretch Goal)
        - May require physics research and/or physics major. (Stretch Goal)
    - Write code for advanced foosball strategies based on future ball location. (Stretch Goal)
* Enemy Tracking (Stretch Goal)
    - 3D print attachment on end of enemy bars (4)
    - Fill attachment with Gyro and accelerometer, location responder, and battery - similar to the gyro foosball. This will track enemy foosball men locations. 
    - Write test cases and code for testing
    - Write code that dynamically changes the map received from the picture, and that changes the expected future location of the ball based on enemy interception. 
    - Test code, refine, retest.

## Team Size at Different Stages:

Stage     | Particular Expertise needed | Justification
----------|------------------------------|---------------
Planning | 1 CE<br />2 MEs                 | CE because software and hardware experience is needed to plan the project at a high-level. MEs to refine the ideas surrounding linear and circular bar movement.
Beginning | 1 CS<br />1 CE/SE | CS Finding foosball men's locations based on photos can begin right away with the help of their phone's camera. CE Gyro sensor experimentation can begin (assuming we have a Gyro sensor and arduino). 
Hardware Obtained | 3 CE/EE<br />2 ME<br />1 CS/SE | Work on Gyro/accelerometer sensors, circular and linear servo bar movement (According to MEs's instruction), and continue camera recognition and mapping.
Hardware Installed | 4 CE/SE/CS | Program becomes primarily software based integrating the hardware into a program that conquers our foosball enemies. 
Stretch Goals | 2 CS<br />someone who loves physics<br />1 CE/EE<br />1 ME |  Print 3D balls (ME), write code to update map received from camera based on enemy tracking, use physics to track future ball position, and write advanced strategies with physics incorporated.
End | Everyone | Test

## Benefits To Interns and to Hill
* Maps small location based on relative distance
* Object detection in images
* Networking
* Harware experience
    - Gyroscope sensor
    - Accelerometer
    - Servos
    - 3D Printing
* Programming with math and physics
* Reactive attacks that implement the best strategy
* Hardware integrated software
