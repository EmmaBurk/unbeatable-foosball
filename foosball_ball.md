## Gyroscope and Acceleromert Setup + Tools
    "The gyroscope measures rotational velocity or rate of change of the angular position over time, along the X, Y and Z axis."
 ~ [Ozeki](https://ozeki.hu/p_2987-how-to-use-a-gyroscope-sensor-in-arduino.html#:~:text=How%20does%20an%20Arduino%20gyroscope,sent%20to%20the%20serial%20port.)

### Set up
<img src="images/Foosball_ball.png" alt="drawing" width="500"/>


# Foosball Ball
Ball can be up to 38mm in diameter.

<img src="images/ball_dimensions.jpg" alt="drawing" width="500"/>


## [Microcontroller](https://www.adafruit.com/product/3500)

<img src="images/microcontroller.png" alt="drawing" width="500"/>


## [Transceiver](https://www.amazon.com/Willwin-Wireless-Transmitter-Receiver-Raspberry/dp/B07DW9K3Q7?th=1)
Serial Communication


    Receiver - Connected to computer:
<img src="images/receiver.PNG" alt="drawing" width="500"/>

    Transmitter - Inside Ball:
<img src="images/transmitter.PNG" alt="drawing" width="500"/>



## [Gyro/Accel](https://makersportal.com/blog/2019/11/11/raspberry-pi-python-accelerometer-gyroscope-magnetometer)
[See Datasheet](https://www.sparkfun.com/datasheets/Components/SMD/nRF24L01Pluss_Preliminary_Product_Specification_v1_0.pdf)

IIC Communication
<img src="images/gyro_accel.png" alt="drawing" width="500"/>
