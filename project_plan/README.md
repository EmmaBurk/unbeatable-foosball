## Project Plan for Unbeatable Foosball at BYU-I
    Stage:         Beginning/Hardware-Obtained
    Dates:         Sept 12th - Dec 16th
    Weekly Hours:  8
    Total Hours:   112
    Semester Cost: $1,788.62        
 
### Specific Goals:
* Proof of Concept with ball
    * Get data from Accelerometer with the adafruit microcontroller and transmit data to computer with a transceiver.
    * Experiment with data received and if it's insufficient then obtain a Gyro-accelerometer and get data from that.
    * Create a 3D ball to house the accelerometer, microcontroller, and transceiver. 
    * Test 3D ball to make sure that data is correctly received. 
* Stretch Goals: 
    * Reduce hardware needed to fit into 3D ball.
    * Forgo microcontroller and accelerometer boards and obtain their main chips. 
    * Integrate the chips together and house materials in a 3D ball. 

### Why not use a Library?
* Requires a different IDE for programming.
* There is not a library that integrates the transceiver with the adafruit microcontroller trinket M0.
* This is objectively cooler. 
    * A library reduces understanding of the hardware.
    * A library produces results by reducing program speed and increasing file size. 


### Specific Plan
Week | To-Do
-----|-------
1 - 2 | Research how to get data from accelerometer - will seek advice from teachers on embedded systems. 
3 - 4 | Get data from accelerometer.
4 | Research transceiver - work with arduino and arduino library to get basic understanding of how it works.
5 | Research transceiver - read the data sheet, look for online tutorials, ask classmates if they've worked with it. 
6 - 7 | Send dummy-data between transmitter and receiver.
8 | Send accelerometer's data through the transmitter to the receiver. 
9 - 11 | Attempt to make sense of data from accelerometer. If accelerometer data is invalid then request gyro-accelerometer.
12 | Update documentation on research
12-14 | Start working with 3D materials to create a ball to house the hardware.

### Benefits to Working at School:
* Access to professor's help
* Embedded Systems classes
* Access to peer help/advice
* Avoid burn-out

### Benefits of the project:
* Bare-metal code which takes a deep look at SPI, USART, and I2C communication which can be reused for any project or to teach different communication protocols.
* Instructions on how to read technical documentation which will enable others to create their own projects on hardware that doesn't have a library.
* Object that maps a small location based on relative distance. 
